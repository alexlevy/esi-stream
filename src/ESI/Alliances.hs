module ESI.Alliances (getAlliances) where

import Conduit as C
import Control.Monad.Catch (MonadMask)
import Control.Retry
import Data.Conduit.Async
import Data.Text as T
import ESI.Config (defaultRetryPolicy)
import ESI.Http
import ESI.Types
import Network.HTTP.Client.Conduit as HCC
import Network.HTTP.Simple
import RIO
import RIO.Orphans ()
import UnliftIO.Concurrent

getAlliances :: (MonadMask m, MonadUnliftIO m, MonadReader env m, HasHttpManager env, HasLogFunc env, HasApiUrl env) => TBQueue Alliance -> CConduit () Alliance m ()
getAlliances q = do
  alliances
    .| concatC
    .| buildRequest
    $=& mapMC (uncurry $ pushAlliance q)
    $=& mapMC (const $ atomically $ readTBQueue q)

alliances :: (MonadIO m, MonadThrow m, MonadReader env m, HasHttpManager env, HasApiUrl env) => ConduitM () Alliances m ()
alliances = do
  esi <- ask <&> apiUrl
  req <- parseRequest $ T.unpack $ esi <> "/alliances"
  resp <- HCC.httpLbs req
  C.yield (getResponseBody resp)
    .| mapMC (unmarshal :: MonadIO m => LByteString -> m [Int])

buildRequest :: (MonadThrow m, MonadReader env m, HasApiUrl env) => ConduitM Int (Int, Request) m ()
buildRequest = mapMC $ \a -> do
  esi <- ask <&> apiUrl
  r <- parseRequest $ T.unpack esi <> "/alliances/" <> show a
  pure (a, r)

pushAlliance :: (MonadMask m, MonadUnliftIO m, MonadReader env m, HasHttpManager env, HasLogFunc env) => TBQueue Alliance -> Int -> Request -> m ()
pushAlliance q i req = do
  forkIO (go >>= atomically . writeTBQueue q) $> ()
  where
    go :: (MonadReader env m, MonadIO m, MonadMask m, HasHttpManager env, HasLogFunc env) => m Alliance
    go = recoverAll defaultRetryPolicy $ \rs -> do
      resp <- HCC.httpLbs req
      logDebug $ "Retry#" <> displayShow (rsIterNumber rs) <> " for " <> displayBytesUtf8 (path req)
      unmarshal (getResponseBody resp) <&> Alliance i . allianceName
