{-# OPTIONS_GHC -Wno-orphans #-}

module Main where

import Conduit
import qualified Data.Aeson as A
import Data.Conduit.Async
import Data.Conduit.List (consume)
import qualified Data.Text.Lazy as T
import Data.Time.Clock
import Database.Persist.Sqlite
import ESI.Alliances
import ESI.Config
import ESI.Systems
import ESI.Types
import Network.HTTP.Client.Conduit (HasHttpManager, Manager, newManagerSettings)
import Network.HTTP.Conduit (tlsManagerSettings)
import Options.Generic
import RIO
import RIO.Orphans ()

instance ParseRecord (EsiStreamOptions Wrapped) where
  parseRecord = parseRecordWithModifiers optModifiers

deriving instance Show (EsiStreamOptions Unwrapped)

optModifiers :: Modifiers
optModifiers = lispCaseModifiers {shortNameModifier = firstLetter}

initHttpManager :: MonadIO m => m Manager
initHttpManager = newManagerSettings tlsManagerSettings

parseOpts :: IO (EsiStreamOptions Unwrapped)
parseOpts = unwrapRecord "ESI Streamer"

writeDefaultConfiguration :: EsiStreamOptions Unwrapped -> IO ()
writeDefaultConfiguration opts = when (writeDefaultConfig opts) $ writeConfiguration defaultConfigurationFile defaultConfig

getEsiStreamConfiguration :: EsiStreamOptions Unwrapped -> IO EsiStreamConfiguration
getEsiStreamConfiguration opts =
  let f = maybe defaultConfigurationFile T.unpack (configFilePath opts)
   in A.eitherDecodeFileStrict f <&> either error id

timed :: HasLogFunc env => Utf8Builder -> RIO env a -> RIO env a
timed s action = do
  t0 <- liftIO getCurrentTime
  x <- action
  t1 <- liftIO getCurrentTime
  logInfo $ s <> " took " <> displayShow (diffUTCTime t1 t0)
  pure x

initDb :: HasDbConfiguration env => RIO env ()
initDb = do
  db <- ask <&> dbName . dbConfig
  runSqlite db $ runMigration migrateAll

clearDb :: HasDbConfiguration env => RIO env ()
clearDb = do
  db <- ask <&> dbName . dbConfig
  runSqlite db $ do
    deleteWhere ([] :: [Filter Alliance])
    deleteWhere ([] :: [Filter System])

insertStuff :: (HasDbConfiguration env, HasQueueSize env, HasLogFunc env, PersistRecordBackend a SqlBackend) => Utf8Builder -> (TBQueue a -> CConduit () a (RIO env) ()) -> RIO env ()
insertStuff s f = do
  env <- ask
  let db = dbName . dbConfig $ env
  q <- newTBQueueIO $ qSize env
  as <- timed ("Fetching " <> s) $ f q $$& consume
  timed ("Inserting " <> s) $ runSqlite db (insertMany_ as)

(∆) :: (HasDbConfiguration env, HasQueueSize env, HasLogFunc env, PersistRecordBackend a SqlBackend) => Utf8Builder -> (TBQueue a -> CConduit () a (RIO env) ()) -> RIO env ()
(∆) = insertStuff

infixl 5 ∆

summarizeDb :: (HasDbConfiguration env, HasLogFunc env) => RIO env ()
summarizeDb = do
  logInfo "+============+"
  logInfo "| DB summary |"
  logInfo "+============+"
  db <- ask <&> dbName . dbConfig
  runSqlite db $ do
    lift . logInfo . (<> " alliances") . displayShow =<< count ([] :: [Filter Alliance])
    lift . logInfo . (<> " systems") . displayShow =<< count ([] :: [Filter System])

loadDb :: (HasDbConfiguration env, HasQueueSize env, HasLogFunc env, HasApiUrl env, HasHttpManager env) => RIO env ()
loadDb = do
  "alliances" ∆ getAlliances
  "systems" ∆ getSystems

main :: IO ()
main = do
  opts <- parseOpts
  writeDefaultConfiguration opts
  cfg <- getEsiStreamConfiguration opts
  logOptions <- setLogUseTime True <$> logOptionsHandle RIO.stderr (verbose opts)
  manager <- initHttpManager

  withLogFunc logOptions $ \lf -> do
    let app = EsiStreamEnv cfg lf manager
    runRIO app $ do
      initDb
      when (initialLoad opts) $ timed "Overall" $ clearDb *> loadDb
      summarizeDb
