import Conduit
import Control.Concurrent
import Data.ByteString.Builder
import RIO

main :: IO ()
main = do
  x <- runConduit $ yieldMany [1 .. 10] .| mapMC parallelMultiply2 .| sumC
  RIO.hPutBuilder RIO.stderr $ stringUtf8 $ "-- Sum : " <> show x

parallelMultiply2 :: Int -> IO Int
parallelMultiply2 i = do
  RIO.hPutBuilder RIO.stderr $ stringUtf8 $ "Value : " <> show i
  v <- newTVarIO i
  t <- forkIO $ atomically $ writeTVar v (i * 2)
  RIO.hPutBuilder RIO.stderr $ stringUtf8 $ " Thread : " <> show t <> "\n"
  readTVarIO v
