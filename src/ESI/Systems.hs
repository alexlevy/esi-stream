module ESI.Systems (getSystems) where

import Conduit as C
import Control.Monad.Catch (MonadMask)
import Control.Retry
import Data.Conduit.Async
import Data.Text as T
import ESI.Config (defaultRetryPolicy)
import ESI.Http
import ESI.Types
import Network.HTTP.Client.Conduit as HCC
import Network.HTTP.Simple
import RIO
import RIO.Orphans ()
import UnliftIO.Concurrent

getSystems :: (MonadMask m, MonadUnliftIO m, MonadReader env m, HasHttpManager env, HasLogFunc env, HasApiUrl env) => TBQueue System -> CConduit () System m ()
getSystems q = do
  systems
    .| concatC
    .| buildRequest
    $=& mapMC (pushSystem q)
    $=& mapMC (const $ atomically $ readTBQueue q)

systems :: (MonadIO m, MonadThrow m, MonadReader env m, HasHttpManager env, HasApiUrl env) => ConduitM () Systems m ()
systems = do
  esi <- ask <&> apiUrl
  req <- parseRequest $ T.unpack $ esi <> "/universe/systems"
  resp <- HCC.httpLbs req
  C.yield (getResponseBody resp)
    .| mapMC (unmarshal :: MonadIO m => LByteString -> m [Int])

buildRequest :: (MonadThrow m, MonadReader env m, HasApiUrl env) => ConduitM Int Request m ()
buildRequest = mapMC $ \a -> do
  esi <- ask <&> apiUrl
  parseRequest $ T.unpack esi <> "/universe/systems/" <> show a

pushSystem :: (MonadMask m, MonadUnliftIO m, MonadReader env m, HasHttpManager env, HasLogFunc env) => TBQueue System -> Request -> m ()
pushSystem q req = do
  forkIO (go >>= atomically . writeTBQueue q) $> ()
  where
    go :: (MonadReader env m, MonadIO m, MonadMask m, HasHttpManager env, HasLogFunc env) => m System
    go = recoverAll defaultRetryPolicy $ \rs -> do
      resp <- HCC.httpLbs req
      logDebug $ "Retry#" <> displayShow (rsIterNumber rs) <> " for " <> displayBytesUtf8 (path req)
      unmarshal (getResponseBody resp)
