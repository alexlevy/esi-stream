module ESI.Http
  ( unmarshal,
    unmarshalStrict,
  )
where

import Conduit
import Data.Aeson
import RIO

unmarshal :: (FromJSON a, MonadIO m) => LByteString -> m a
unmarshal bs = let v = eitherDecode bs in either RIO.throwString pure v

unmarshalStrict :: (FromJSON a, MonadIO m) => ByteString -> m a
unmarshalStrict bs = let v = eitherDecodeStrict bs in either RIO.throwString pure v
