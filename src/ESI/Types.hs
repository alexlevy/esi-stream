{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-duplicate-exports #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module ESI.Types
  ( Alliance (..),
    System (..),
    Alliances,
    Systems,
    EsiDataSource (..),
    EsiStreamConfiguration (..),
    DbConfiguration (..),
    EsiStreamEnv (..),
    EsiStreamOptions (..),
    HasDbConfiguration,
    HasQueueSize,
    HasApiUrl,
    migrateAll,
    module ESI.Types,
  )
where

import Data.Aeson.Types
import qualified Data.Text.Lazy as T
import Database.Persist.Sqlite
import Database.Persist.TH
import Network.HTTP.Client.Conduit (HasHttpManager (..))
import Network.HTTP.Conduit (Manager)
import Options.Generic
import RIO

data EsiStreamOptions w = EsiStreamOptions
  { configFilePath :: w ::: Maybe T.Text <?> "Configuration file path",
    writeDefaultConfig :: w ::: Bool <?> "Write default configuration file",
    verbose :: w ::: Bool <?> "Verbose output",
    initialLoad :: w ::: Bool <?> "Load data to database"
  }
  deriving (Generic)

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
  Alliance
    allianceId Int
    name Text
    UniqueAlliance name
    deriving Generic Show
  System
    systemId Int
    name Text
    UniqueSystem name
    x Double
    y Double
    z Double
    securityClass Text Maybe
    securityStatus Double
    starId Int Maybe
    constellationId Int
    deriving Generic Show
|]

type Alliances = [Int]

type Systems = [Int]

instance FromJSON Alliance where
  parseJSON (Object v) = Alliance <$> v .:? "_aid" .!= (-1) <*> v .: "name"
  parseJSON invalid = prependFailure "Parsing Alliance failed" (typeMismatch "object" invalid)

instance FromJSON System where
  parseJSON (Object v) =
    System
      <$> v .: "system_id"
      <*> v .: "name"
      <*> (v .: "position" >>= (.: "x"))
      <*> (v .: "position" >>= (.: "y"))
      <*> (v .: "position" >>= (.: "z"))
      <*> v .:? "security_class"
      <*> v .: "security_status"
      <*> v .:? "star_id"
      <*> v .: "constellation_id"
  parseJSON invalid = prependFailure "Parsing System failed" (typeMismatch "object" invalid)

data EsiStreamEnv = EsiStreamEnv
  { esiStreamConf :: !EsiStreamConfiguration,
    logFunc :: !RIO.LogFunc,
    httpManager :: !Manager
  }

data EsiStreamConfiguration = EsiStreamConfiguration
  { esiApiConfiguration :: !EsiApiConfiguration,
    dbConfiguration :: !DbConfiguration,
    queueSize :: !Natural
  }
  deriving (Show, Generic)
  deriving anyclass (FromJSON, ToJSON)

data EsiApiConfiguration = EsiApiConfiguration
  { url :: Text,
    dataSource :: EsiDataSource
  }
  deriving (Show, Generic)
  deriving anyclass (FromJSON, ToJSON)

newtype DbConfiguration = DbConfiguration {dbName :: Text} deriving (Show, Generic, Eq)

instance FromJSON DbConfiguration

instance ToJSON DbConfiguration

data EsiDataSource = Tranquility
  deriving (Show, Generic, Eq)

instance ToJSON EsiDataSource where
  toJSON ds = case ds of
    Tranquility -> String "tranquility"

instance FromJSON EsiDataSource where
  parseJSON v = withText "EsiDataSource" (\ds -> if ds == "tranquility" then return Tranquility else unexpected v) v

instance HasLogFunc EsiStreamEnv where
  logFuncL = lens logFunc (\x y -> x {logFunc = y})

instance HasHttpManager EsiStreamEnv where
  getHttpManager = httpManager

class HasDbConfiguration env where
  dbConfig :: env -> DbConfiguration

instance HasDbConfiguration DbConfiguration where
  dbConfig = id

instance HasDbConfiguration EsiStreamConfiguration where
  dbConfig = dbConfiguration

instance HasDbConfiguration EsiStreamEnv where
  dbConfig = dbConfig . esiStreamConf

class HasApiUrl env where
  apiUrl :: env -> Text

instance HasApiUrl EsiApiConfiguration where
  apiUrl = url

instance HasApiUrl EsiStreamEnv where
  apiUrl = url . esiApiConfiguration . esiStreamConf

class HasQueueSize env where
  qSize :: env -> Natural

instance HasQueueSize EsiStreamConfiguration where
  qSize = queueSize

instance HasQueueSize EsiStreamEnv where
  qSize = qSize . esiStreamConf
