module ESI.Config
  ( defaultConfig,
    writeConfiguration,
    defaultConfigurationFile,
    defaultRetryPolicy,
  )
where

import Control.Retry
import Data.Aeson.Encode.Pretty (encodePretty)
import Data.Text.Lazy.Encoding (decodeUtf8)
import ESI.Types
import RIO
import RIO.Text.Lazy (toStrict)

defaultConfig :: EsiStreamConfiguration
defaultConfig =
  EsiStreamConfiguration
    { esiApiConfiguration = EsiApiConfiguration "https://esi.evetech.net/latest" Tranquility,
      dbConfiguration = DbConfiguration "esi_stream.sqlite",
      queueSize = 50
    }

writeConfiguration :: FilePath -> EsiStreamConfiguration -> IO ()
writeConfiguration f c = let t = decodeUtf8 (encodePretty c) in writeFileUtf8 f (toStrict t)

defaultConfigurationFile :: FilePath
defaultConfigurationFile = "default.json"

defaultRetryPolicy :: MonadIO m => RetryPolicyM m
defaultRetryPolicy = fullJitterBackoff 50_000 <> limitRetries 5
