# ESI streaming
 
# Security system color calculation
* b = 0 by default
* if ss >= 0.5 then g = 255 ; r = 2 * (255 - (ss * 255))
* if ss < 0.5 then r = 255 ; g = 1.5 * ss * 255
* if ss < 0 then r = 255 ; g = 0
* multiply r and g by a V (value) to decrease brightness
* add some b to decrease saturation
